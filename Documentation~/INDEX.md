<h1>Build Management Documentation</h1>

With this package you can automatize some common tasks like removing the splash screen, increasing the build number and generate build output with a certain pattern.

There are settings that affect all the developers in the project - and so enforcing everyone being on the same page - and settings that can be individually set by each user on its machine.

- [API](#api)
  - [`BuildMetadata`](#buildmetadata)
  - [`UnityCloudBuildManifest`](#unitycloudbuildmanifest)
- [Project Settings Scope](#project-settings-scope)
  - [General](#general)
  - [Local Builds](#local-builds)
  - [Unity Cloud Build](#unity-cloud-build)
  - [Version](#version)
- [User Preferences Scope](#user-preferences-scope)
  - [Local Builds](#local-builds-1)

---

## API

---

### `BuildMetadata`

Holds custom metadata injected during the build.

That custom metadata is visible in the Unity Cloud Diagnostics when a new crash or exception is received.

| Member | Description |
| :--- | :--- |
| BuildName | The build name as chosen in the project settings. |
| FullVersion | The unique version of the build in the format "{bundleVersion}-{dateTime}". |
| GetInstance | Use this to access the custom build metadata.<br/>Returns null if the custom build metadata could not be found. |

### `UnityCloudBuildManifest`
[UnityCloudBuildManifest]:https://docs.unity3d.com/Manual/UnityCloudBuildManifest.html

Use this to quick access the [UnityCloudBuildManifest] json.

This is just a wrapper class so that you don't need to remember each property name to access it.

| Member | Description |
| :--- | :--- |
| BuildNumber | The Unity Cloud Build “build number” corresponding to this build. |
| BuildStartTime | The UTC timestamp when the build process was started. |
| BundleId | The bundleIdentifier configured in Unity Cloud Build (iOS and Android only). |
| CloudBuildTargetName | The name of the build target that was built. |
| ProjectId | The Unity project identifier. |
| ScmBranch | The name of the branch that was built. |
| ScmCommitId | The commit or change-list that was built. |
| UnityVersion | The version of Unity that Unity Cloud Build used to create the build. |
| XcodeVersion | The version of XCode used to build the project (iOS only). |
| GetInstance | Use this to access the [UnityCloudBuildManifest] json. |

---

## Project Settings Scope

---

As any other project setting you can change those in `Edit/Project Settings...` then click on `Build Manager` on the left column.

![](project-settings.png)

### General

Global utilities for when generating a build.

| Property | Function |
| :--- | :--- |
| Show Splash Screen | If you are a Plus/Pro subscriber this will override the `Player` settings. Useful if working on a project in which not all developers have an active subscription. |
| Show Unity Logo | If you are a Plus/Pro subscriber this will override the `Player` settings. Useful if working on a project in which not all developers have an active subscription.|
| Use Utc Now as iOS Build Number | If checked the `iOS Build Number` will be the build time in UTC (Coordinated Universal Time). The build number will be in the format `"yyyy.MMdd.HHmm"` thus ensuring an ever-increasing and ever-changing `Build Number`. |
| Use Utc Now as Mac Build Number | If checked the `Mac Build Number` will be the build time in UTC (Coordinated Universal Time). The build number will be in the format `"yyyy.MMdd.HHmm"` thus ensuring an ever-increasing and ever-changing `Build Number`. |

### Local Builds

The settings here will **not** take effect in the Unity Cloud Build.

| Property | Function |
| :--- | :--- |
| Use Project Folder As Build Name | If checked the standardized build output will have the project's folder name instead of using the `Product Name`.<br/>**WARNING:** This setting will only take affect if [`Create Standardized Build Output`](#local-builds-1) is check. Also this setting is shared within the project itself as this ensures that all users will be using the same configuration. |
| Override Scripting Backend | If checked you can choose which scripting backend to be used without needing to remember to change back on the `Player` settings every time. If IL2PP is not supported for the current selected platform in your machine it will automatically fallback to Mono. |
| Preferred Linux Scripting Backend | The preferred scripting backend to be used when building for Linux. |
| Preferred Mac Scripting Backend | The preferred scripting backend to be used when building for Mac. |
| Preferred Windows Scripting Backend | The preferred scripting backend to be used when building for Windows. |

### Unity Cloud Build

Those settings will only affect builds generated with the Unity Cloud Build.

| Property | Function |
| :--- | :--- |
| Sum Build Number to Bundle Version Code | If checked the `Android Bundle Version Code` will be a sum of the current `Bundle Version Code` in the `Player` settings with the `Build Number` from the build being generated on the Unity Cloud Build. This will ensure an ever-increasing and ever-changing `Bundle Version Code`. |
| Use Build Start Time as Utc Now | If checked it will override the UTC (Coordinated Universal Time) information with the actual `Build Start Time` information from the Unity Cloud Build. This is a good practice as the original UTC can be out-of-sync due all the pre-process stuff that the Unity Cloud Build does. |

### Version

Settings to change the version output on the builds.

| Property | Function |
| :--- | :--- |
| Override Version | If checked it will override the version from the `Player` settings with the one specified on that window. Useful to ensure a standard versioning across all builds as this will always produce a version with the format `"Major.Minor.Patch"`. |
| Major Version | The `Major` version number to be used on the overridden version. | 
| Minor Version | The `Minor` version number to be used on the overridden version. |
| Patch Version | The `Patch` version number to be used on the overridden version. |

---

## User Preferences Scope

---

As any other user preference you can change those in `Edit/Preferences...` then click on `Build Manager` on the left column.

![](preferences.png)

### Local Builds

The settings here will **not** take effect in the Unity Cloud Build.

| Property | Function |
| :--- | :--- |
| Open Built Player Options | By default Unity's show the built player folder after the building process. With this option you can choose to see both the original output folder and the standardized output, only one of them or even none of them at all. |
| Create Standardized Build Output | If checked it will copy the built player into the selected folder with a standardized name. |
| Group by Build Name | If checked the copied output will be put inside a folder with the same name as the build name. This way you can choose to have one build folder for all your projects without becoming starting mess. |
| Group by Build Target | If checked the copied output will be put inside a folder with the name of the build target. If `Group by Build Name` is also checked the build target folder folder will be placed inside the build name. |
| Standardized Build Output Path | Click `Change Path` to choose the folder to place your standardized builds. At any time you can click in the path to open the folder.<br/>The default value is a folder named `Builds` inside the relative project. |
