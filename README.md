# Build Management

Automatize some common actions when building the application.

## See also

- [Documentation](Documentation~/INDEX.md)
- [Changelog](CHANGELOG.md)
- [License](LICENSE.md)
- [Author](https://gitlab.com/brunocoimbra)
