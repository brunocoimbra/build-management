using UnityEditor.SettingsManagement;

namespace PainfulSmile.BuildManagement
{
    internal sealed class PackageSetting<T> : UserSetting<T>
    {
        internal PackageSetting(string key, T value)
            : base(PackageSettingsProvider.Settings, key, value) { }
    }
}
