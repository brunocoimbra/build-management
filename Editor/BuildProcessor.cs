using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace PainfulSmile.BuildManagement
{
    internal sealed class BuildProcessor : IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
        int IOrderedCallback.callbackOrder => 0;

        void IPostprocessBuildWithReport.OnPostprocessBuild(BuildReport report) { }

        void IPreprocessBuildWithReport.OnPreprocessBuild(BuildReport report)
        {
            BuildManager.ApplySettings();
        }
    }
}
