using System;
using UnityEditor;

namespace PainfulSmile.BuildManagement
{
    internal static class BuildManager
    {
        internal static void ApplySettings()
        {
            DateTime utcNowRaw = DateTime.UtcNow;
            PlayerSettings.SplashScreen.show = PackageSettingsProvider.ShowSplashScreen;
            PlayerSettings.SplashScreen.showUnityLogo = PackageSettingsProvider.ShowUnityLogo;
            UnityCloudBuildManifest manifest = UnityCloudBuildManifest.GetInstance();

            if (manifest != null)
            {
                if (PackageSettingsProvider.UseBuildStartTimeAsUtcNow)
                {
                    utcNowRaw = DateTime.Parse(manifest.BuildStartTime);
                }
#if UNITY_ANDROID
                if (PackageSettingsProvider.SumBuildNumberToBundleVersionCode)
                {
                    ExpressionEvaluator.Evaluate(manifest.BuildNumber, out int result);
                    PlayerSettings.Android.bundleVersionCode += result;
                }
#endif
            }

            string utcNow = utcNowRaw.ToString("yyyy.MMdd.HHmm");
#if UNITY_IOS
            if (PackageSettingsProvider.UseUtcNowAsIosBuildNumber)
            {
                PlayerSettings.iOS.buildNumber = utcNow;
            }
#elif UNITY_STANDALONE_OSX
            if (PackageSettingsProvider.UseUtcNowAsMacBuildNumber)
            {
                PlayerSettings.macOS.buildNumber = utcNow;
            }
#endif
            if (PackageSettingsProvider.OverrideVersion)
            {
                PlayerSettings.bundleVersion = PackageSettingsProvider.Version;
            }

            const ImportAssetOptions options = ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport;

            BuildMetadata.SetInstance(BuildManagerUtility.BuildName, $"{PlayerSettings.bundleVersion}-{utcNow}");
            AssetDatabase.ImportAsset($"Assets/StreamingAssets/{BuildMetadata.FileName}", options);
        }
    }
}
