using UnityEditor;
using UnityEditor.SettingsManagement;
using UnityEngine;

namespace PainfulSmile.BuildManagement
{
    internal static class PackageSettingsProvider
    {
        private enum ScriptingBackend
        {
            Mono,
            IL2CPP
        }

        private static readonly GUIContent PreferredLinuxScriptingBackendLabel = new GUIContent("Preferred Linux Scripting Backend");
        private static readonly GUIContent PreferredMacScriptingBackendLabel = new GUIContent("Preferred Mac Scripting Backend");
        private static readonly GUIContent PreferredWindowsScriptingBackendLabel = new GUIContent("Preferred Windows Scripting Backend");
        private static readonly GUIContent ShowUserPreferencesLabel = new GUIContent("Show User Preferences");

        [UserSetting] private static readonly PackageSetting<bool> ShowSplashScreenSetting = new PackageSetting<bool>("general.showSplashScreen", true);
        [UserSetting] private static readonly PackageSetting<bool> ShowUnityLogoSetting = new PackageSetting<bool>("general.showUnityLogo", false);
        [UserSetting] private static readonly PackageSetting<bool> UseUtcNowAsIosBuildNumberSetting = new PackageSetting<bool>("general.useUtcNowAsIosBuildNumber", true);
        [UserSetting] private static readonly PackageSetting<bool> UseUtcNowAsMacBuildNumberSetting = new PackageSetting<bool>("general.useUtcNowAsMacBuildNumber", true);
        [UserSetting] private static readonly PackageSetting<bool> OverrideVersionSetting = new PackageSetting<bool>("version.overrideVersion", true);
        [UserSetting] private static readonly PackageSetting<int> MajorVersionSetting = new PackageSetting<int>("version.majorVersion", 0);
        [UserSetting] private static readonly PackageSetting<int> MinorVersionSetting = new PackageSetting<int>("version.minorVersion", 1);
        [UserSetting] private static readonly PackageSetting<int> PatchVersionSetting = new PackageSetting<int>("version.patchVersion", 0);
        [UserSetting] private static readonly PackageSetting<bool> UseProjectFolderAsBuildNameSetting = new PackageSetting<bool>("localBuilds.useProjectFolderAsBuildName", false);
        [UserSetting] private static readonly PackageSetting<bool> OverrideScriptingBackendSetting = new PackageSetting<bool>("localBuilds.overrideScriptingBackend", true);
        [UserSetting] private static readonly PackageSetting<ScriptingBackend> PreferredLinuxScriptingBackendSetting = new PackageSetting<ScriptingBackend>("localBuilds.preferredLinuxScriptingBackend", ScriptingBackend.IL2CPP);
        [UserSetting] private static readonly PackageSetting<ScriptingBackend> PreferredMacScriptingBackendSetting = new PackageSetting<ScriptingBackend>("localBuilds.preferredMacScriptingBackend", ScriptingBackend.IL2CPP);
        [UserSetting] private static readonly PackageSetting<ScriptingBackend> PreferredWindowsScriptingBackendSetting = new PackageSetting<ScriptingBackend>("localBuilds.preferredWindowsScriptingBackend", ScriptingBackend.IL2CPP);
        [UserSetting("Unity Cloud Build", "Sum Build Number to Bundle Version Code")]
        private static readonly PackageSetting<bool> SumBuildNumberToBundleVersionCodeSetting = new PackageSetting<bool>("unityCloudBuild.sumBuildNumberToBundleVersionCode", true);
        [UserSetting("Unity Cloud Build", "Use Build Start Time as Utc Now")]
        private static readonly PackageSetting<bool> UseBuildStartTimeAsUtcNowSetting = new PackageSetting<bool>("unityCloudBuild.useBuildStartTimeAsUtcNow", true);

        private static Settings s_Settings;

        internal static bool OverrideVersion => OverrideVersionSetting;
        internal static bool ShowSplashScreen => ShowSplashScreenSetting;
        internal static bool ShowUnityLogo => ShowUnityLogoSetting;
        internal static bool OverrideScriptingBackend => OverrideScriptingBackendSetting;
        internal static bool SumBuildNumberToBundleVersionCode => SumBuildNumberToBundleVersionCodeSetting;
        internal static bool UseBuildStartTimeAsUtcNow => UseBuildStartTimeAsUtcNowSetting;
        internal static bool UseProjectFolderAsBuildName => UseProjectFolderAsBuildNameSetting;
        internal static bool UseUtcNowAsIosBuildNumber => UseUtcNowAsIosBuildNumberSetting;
        internal static bool UseUtcNowAsMacBuildNumber => UseUtcNowAsMacBuildNumberSetting;
        internal static string Version => $"{MajorVersionSetting.value}.{MinorVersionSetting.value}.{PatchVersionSetting.value}";
        internal static ScriptingImplementation PreferredLinuxScriptingBackend => (ScriptingImplementation)PreferredLinuxScriptingBackendSetting.value;
        internal static ScriptingImplementation PreferredMacScriptingBackend => (ScriptingImplementation)PreferredMacScriptingBackendSetting.value;
        internal static ScriptingImplementation PreferredWindowsScriptingBackend => (ScriptingImplementation)PreferredWindowsScriptingBackendSetting.value;

        internal static Settings Settings => s_Settings ?? (s_Settings = new Settings(BuildManagerUtility.PackageName));

        [SettingsProvider]
        private static SettingsProvider CreateSettingsProvider()
        {
            return new UserSettingsProvider(BuildManagerUtility.ProjectSettingsPath, Settings, new[] { typeof(PackageSettingsProvider).Assembly }, SettingsScope.Project);
        }

        [UserSettingBlock(" ")]
        private static void DrawHeaderBlock(string searchContext)
        {
            if (!BuildManagerUtility.TryMatchSearch(searchContext, ShowUserPreferencesLabel.text))
            {
                return;
            }

            if (GUILayout.Button(ShowUserPreferencesLabel))
            {
                SettingsService.OpenUserPreferences(BuildManagerUtility.UserPreferencesPath);
            }
        }

        [UserSettingBlock("General")]
        private static void DrawGeneralBlock(string searchContext)
        {
            using (var blockScope = new EditorGUI.ChangeCheckScope())
            {
                ShowSplashScreenSetting.value = SettingsGUILayout.SettingsToggle("Show Splash Screen", ShowSplashScreenSetting, searchContext);

                using (new EditorGUI.IndentLevelScope())
                using (new EditorGUI.DisabledScope(!ShowSplashScreenSetting))
                {
                    ShowUnityLogoSetting.value = SettingsGUILayout.SettingsToggle("Show Unity Logo", ShowUnityLogoSetting, searchContext);
                }

                UseUtcNowAsIosBuildNumberSetting.value = SettingsGUILayout.SettingsToggle("Use Utc Now as iOS Build Number", UseUtcNowAsIosBuildNumberSetting, searchContext);
                UseUtcNowAsMacBuildNumberSetting.value = SettingsGUILayout.SettingsToggle("Use Utc Now as Mac Build Number", UseUtcNowAsMacBuildNumberSetting, searchContext);

                if (blockScope.changed)
                {
                    Settings.Save();
                }
            }
        }

        [UserSettingBlock("Local Builds")]
        private static void DrawLocalBuildsBlock(string searchContext)
        {
            using (var blockScope = new EditorGUI.ChangeCheckScope())
            {
                UseProjectFolderAsBuildNameSetting.value = SettingsGUILayout.SettingsToggle("Use Project Folder as Build Name", UseProjectFolderAsBuildNameSetting, searchContext);
                OverrideScriptingBackendSetting.value = SettingsGUILayout.SettingsToggle("Override Scripting Backend", OverrideScriptingBackendSetting, searchContext);

                using (new EditorGUI.IndentLevelScope())
                using (new EditorGUI.DisabledScope(!OverrideScriptingBackendSetting))
                {
                    if (BuildManagerUtility.TryMatchSearch(searchContext, PreferredLinuxScriptingBackendLabel.text))
                    {
                        PreferredLinuxScriptingBackendSetting.value = (ScriptingBackend)EditorGUILayout.EnumPopup(PreferredLinuxScriptingBackendLabel, PreferredLinuxScriptingBackendSetting);
                        SettingsGUILayout.DoResetContextMenuForLastRect(PreferredLinuxScriptingBackendSetting);
                    }

                    if (BuildManagerUtility.TryMatchSearch(searchContext, PreferredMacScriptingBackendLabel.text))
                    {
                        PreferredMacScriptingBackendSetting.value = (ScriptingBackend)EditorGUILayout.EnumPopup(PreferredMacScriptingBackendLabel, PreferredMacScriptingBackendSetting);
                        SettingsGUILayout.DoResetContextMenuForLastRect(PreferredMacScriptingBackendSetting);
                    }

                    if (BuildManagerUtility.TryMatchSearch(searchContext, PreferredWindowsScriptingBackendLabel.text))
                    {
                        PreferredWindowsScriptingBackendSetting.value = (ScriptingBackend)EditorGUILayout.EnumPopup(PreferredWindowsScriptingBackendLabel, PreferredWindowsScriptingBackendSetting);
                        SettingsGUILayout.DoResetContextMenuForLastRect(PreferredWindowsScriptingBackendSetting);
                    }
                }

                if (blockScope.changed)
                {
                    Settings.Save();
                }
            }
        }

        [UserSettingBlock("Version")]
        private static void DrawVersionBlock(string searchContext)
        {
            using (var blockScope = new EditorGUI.ChangeCheckScope())
            {
                OverrideVersionSetting.value = SettingsGUILayout.SettingsToggle("Override Version", OverrideVersionSetting, searchContext);

                using (new EditorGUI.DisabledScope(!OverrideVersionSetting))
                using (new SettingsGUILayout.IndentedGroup())
                {
                    MajorVersionSetting.value = SettingsGUILayout.SettingsIntField("Major Version", MajorVersionSetting, searchContext);
                    MinorVersionSetting.value = SettingsGUILayout.SettingsIntField("Minor Version", MinorVersionSetting, searchContext);
                    PatchVersionSetting.value = SettingsGUILayout.SettingsIntField("Patch Version", PatchVersionSetting, searchContext);
                }

                if (blockScope.changed)
                {
                    Settings.Save();
                }
            }
        }
    }
}
