using System;

namespace PainfulSmile.BuildManagement.Local
{
    [Flags]
    internal enum OpenBuiltPlayerOptions
    {
        DontOpen = 0,
        OpenOriginalOutput = 1,
        OpenStandardizedOutput = 2,
        OpenBoth = ~0
    }
}
