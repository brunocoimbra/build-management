using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace PainfulSmile.BuildManagement.Local
{
    internal static class BuildPlayerHandler
    {
        private sealed class ThreadState
        {
            public readonly bool ShowNewOutput;
            public readonly string BuildVersion;
            public readonly string NewOutputFileName;
            public readonly string NewOutputFolderPath;
            public readonly string OldOutputPath;
            public readonly string ProductName;
            public readonly BuildTarget BuildTarget;

            public ThreadState(BuildSummary buildSummary, BuildMetadata buildMetadata, bool showNewOutput)
            {
                ShowNewOutput = showNewOutput;
                BuildTarget = buildSummary.platform;
                OldOutputPath = buildSummary.outputPath;
                ProductName = PlayerSettings.productName;
                BuildVersion = $"v{buildMetadata?.FullVersion ?? PlayerSettings.bundleVersion}";
                NewOutputFolderPath = $"{LocalSettingsProvider.StandardizedBuildOutputPath}";
                NewOutputFileName = buildMetadata?.BuildName ?? BuildManagerUtility.BuildName;

                if (LocalSettingsProvider.GroupByBuildName)
                {
                    NewOutputFolderPath = $"{NewOutputFolderPath}/{NewOutputFileName}";
                }

                if (LocalSettingsProvider.GroupByBuildTarget)
                {
                    NewOutputFolderPath = $"{NewOutputFolderPath}/{BuildTarget}";
                }

                NewOutputFolderPath = $"{NewOutputFolderPath}/{NewOutputFileName} ({BuildTarget}) {BuildVersion}";
            }
        }

#if !UNITY_CLOUD_BUILD
        [InitializeOnLoadMethod]
        private static void Initialize()
        {
            BuildPlayerWindow.RegisterBuildPlayerHandler(HandleBuildPlayer);
        }
#endif

        private static bool AssertBuildSucceeded(BuildSummary buildSummary)
        {
            string message = $"Build completed with a result of '{buildSummary.result:g}'";

            switch (buildSummary.result)
            {
                case BuildResult.Unknown:
                {
                    Debug.LogWarning(message);

                    return false;
                }

                case BuildResult.Failed:
                {
                    Debug.LogError(message);

                    return false;
                }

                case BuildResult.Succeeded:
                {
                    Debug.Log(message);

                    return true;
                }

                case BuildResult.Cancelled:
                {
                    Debug.Log(message);

                    return false;
                }

                default:
                    throw new ArgumentOutOfRangeException(nameof(buildSummary.result), $"{buildSummary.result}");
            }
        }

        private static void CopyAllFiles(string oldOutputPath, string newOutputPath)
        {
            if (Directory.Exists(newOutputPath))
            {
                Directory.Delete(newOutputPath, true);
            }

            foreach (string file in Directory.EnumerateFiles(oldOutputPath, "*", SearchOption.AllDirectories))
            {
                string to = $"{newOutputPath}/{file.Remove(0, oldOutputPath.Length)}";
                BuildManagerUtility.EnsureDirectoryExists(Path.GetDirectoryName(to));
                File.Copy(file, to);
            }
        }

        private static void CopyAllFiles(string oldOutputPath, string newOutputFolderPath, string newOutputFileName)
        {
            if (Directory.Exists(newOutputFolderPath))
            {
                Directory.Delete(newOutputFolderPath, true);
            }

            string oldOutputFolderPath = Path.GetDirectoryName(oldOutputPath);
            string oldOutputFileName = Path.GetFileNameWithoutExtension(oldOutputPath);
            string ignoredFolder = $"{oldOutputFileName}_BackUpThisFolder_ButDontShipItWithYourGame";

            foreach (string file in Directory.EnumerateFiles(oldOutputFolderPath, "*", SearchOption.AllDirectories))
            {
                string relativePath = file.Remove(0, oldOutputFolderPath.Length + 1);
                string to;

                if (relativePath.StartsWith(oldOutputFileName))
                {
                    if (relativePath.StartsWith(ignoredFolder))
                    {
                        continue;
                    }

                    to = $"{newOutputFolderPath}/{newOutputFileName}{relativePath.Remove(0, oldOutputFileName.Length)}";
                }
                else
                {
                    to = $"{newOutputFolderPath}/{relativePath}";
                }

                BuildManagerUtility.EnsureDirectoryExists(Path.GetDirectoryName(to));
                File.Copy(file, to);
            }
        }

        private static void CopyAndroidFiles(string oldOutputPath, string newOutputPath, string productName)
        {
            string folder = Path.GetDirectoryName(newOutputPath);

            if (Directory.Exists(folder))
            {
                Directory.Delete(folder, true);
            }

            Directory.CreateDirectory(folder);

            string extension = Path.GetExtension(oldOutputPath);

            if (string.IsNullOrWhiteSpace(extension))
            {
                const string arm64 = ".arm64-v8a.apk";
                const string armeabi = ".armeabi-v7a.apk";

                File.Copy($"{oldOutputPath}/{productName}{arm64}", $"{newOutputPath}{arm64}");
                File.Copy($"{oldOutputPath}/{productName}{armeabi}", $"{newOutputPath}{armeabi}");
            }
            else
            {
                File.Copy(oldOutputPath, $"{newOutputPath}{extension}");
            }
        }

        private static void CreateStandardizedBuildOutput(object state)
        {
            Debug.Log($"Generating standardized build output.");

            try
            {
                var threadState = (ThreadState)state;

                switch (threadState.BuildTarget)
                {
                    case BuildTarget.StandaloneWindows:
                    case BuildTarget.StandaloneWindows64:
                    {
                        CopyAllFiles(threadState.OldOutputPath, threadState.NewOutputFolderPath, threadState.NewOutputFileName);

                        break;
                    }

                    case BuildTarget.StandaloneOSX:
                    {
                        string newOutputPath = $"{threadState.NewOutputFolderPath}/{threadState.NewOutputFileName}.app";
                        CopyAllFiles(threadState.OldOutputPath, newOutputPath);

                        break;
                    }

                    case BuildTarget.StandaloneLinux64:
                    {
                        CopyAllFiles(threadState.OldOutputPath, threadState.NewOutputFolderPath, threadState.NewOutputFileName);

                        break;
                    }

                    case BuildTarget.Android:
                    {
                        string newOutputPath = $"{threadState.NewOutputFolderPath}/{threadState.NewOutputFileName} {threadState.BuildVersion}";
                        CopyAndroidFiles(threadState.OldOutputPath, newOutputPath, threadState.ProductName);

                        break;
                    }

                    case BuildTarget.iOS:
                    case BuildTarget.tvOS:
                    {
                        CopyAllFiles(threadState.OldOutputPath, threadState.NewOutputFolderPath);

                        break;
                    }

                    case BuildTarget.WebGL:
                    {
                        string newOutputPath = $"{threadState.NewOutputFolderPath}/WebGL";
                        CopyAllFiles(threadState.OldOutputPath, newOutputPath);

                        break;
                    }
                }

                Debug.Log($"Generated standardized build output at {threadState.NewOutputFolderPath}");

                if (threadState.ShowNewOutput)
                {
                    Process.Start(threadState.NewOutputFolderPath);
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Debug.LogError("Failed to generate standardized build output.");
            }
        }

        private static void HandleBuildPlayer(BuildPlayerOptions buildPlayerOptions)
        {
            if (string.IsNullOrEmpty(buildPlayerOptions.locationPathName))
            {
                return;
            }

            if (buildPlayerOptions.target == BuildTarget.WebGL && Path.GetFileName(buildPlayerOptions.locationPathName) != "WebGL")
            {
                buildPlayerOptions.locationPathName = $"{buildPlayerOptions.locationPathName}/WebGL";
            }

            bool ignoreStandardizedOutput = !LocalSettingsProvider.CreateStandardizedBuildOutput
#if UNITY_2019_3_OR_NEWER && UNITY_STANDALONE_OSX
                                         || UnityEditor.OSXStandalone.UserBuildSettings.createXcodeProject
#endif
#if UNITY_STANDALONE_WIN
                                         || UnityEditor.WindowsStandalone.UserBuildSettings.createSolution
#endif
                                         || EditorUserBuildSettings.exportAsGoogleAndroidProject;

            switch (buildPlayerOptions.target)
            {
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                {
                    if (PackageSettingsProvider.OverrideScriptingBackend)
                    {
#if UNITY_STANDALONE_WIN
                        PlayerSettings.SetScriptingBackend(buildPlayerOptions.targetGroup, PackageSettingsProvider.PreferredWindowsScriptingBackend);
#else
                        PlayerSettings.SetScriptingBackend(buildPlayerOptions.targetGroup, ScriptingImplementation.Mono2x);
#endif
                    }

                    break;
                }

                case BuildTarget.StandaloneOSX:
                {
                    if (PackageSettingsProvider.OverrideScriptingBackend)
                    {
#if UNITY_STANDALONE_OSX
                        PlayerSettings.SetScriptingBackend(buildPlayerOptions.targetGroup, PackageSettingsProvider.PreferredMacScriptingBackend);
#else
                        PlayerSettings.SetScriptingBackend(buildPlayerOptions.targetGroup, ScriptingImplementation.Mono2x);
#endif
                    }

                    break;
                }

                case BuildTarget.StandaloneLinux64:
                {
                    if (PackageSettingsProvider.OverrideScriptingBackend)
                    {
#if UNITY_STANDALONE_LINUX
                        PlayerSettings.SetScriptingBackend(buildPlayerOptions.targetGroup, PackageSettingsProvider.PreferredLinuxScriptingBackend);
#else
                        PlayerSettings.SetScriptingBackend(buildPlayerOptions.targetGroup, ScriptingImplementation.Mono2x);
#endif
                    }

                    break;
                }

                case BuildTarget.Android:
                case BuildTarget.iOS:
                case BuildTarget.tvOS:
                case BuildTarget.WebGL:
                    break;

                default:
                {
                    ignoreStandardizedOutput = true;

                    Debug.Log($"Generating a standardized build output for {buildPlayerOptions.target} is not supported.");

                    break;
                }
            }

            OpenBuiltPlayerOptions openBuiltPlayerOptions = LocalSettingsProvider.OpenBuiltPlayerOptions;
            bool autoRunPlayer = (buildPlayerOptions.options & BuildOptions.AutoRunPlayer) != 0;
            bool openOriginalOutput = (openBuiltPlayerOptions & OpenBuiltPlayerOptions.OpenOriginalOutput) != 0;
            bool openStandardizedOutput = (openBuiltPlayerOptions & OpenBuiltPlayerOptions.OpenStandardizedOutput) != 0;

            openOriginalOutput = openOriginalOutput || (ignoreStandardizedOutput && openStandardizedOutput);

            if (autoRunPlayer || !openOriginalOutput)
            {
                buildPlayerOptions.options &= ~BuildOptions.ShowBuiltPlayer;
            }

            BuildReport buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);
            BuildMetadata buildMetadata = BuildMetadata.GetInstance();

            if (buildMetadata != null)
            {
                try
                {
                    AssetDatabase.DeleteAsset($"Assets/StreamingAssets/{BuildMetadata.FileName}");
                    string streamingAssetsPath = Application.streamingAssetsPath;

                    if (buildMetadata.InternalFlag)
                    {
                        Directory.Delete(streamingAssetsPath, true);
                        File.Delete($"{streamingAssetsPath}.meta");
                    }
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e.Message);
                }
            }

            if (buildReport == null)
            {
                return;
            }

            BuildSummary buildSummary = buildReport.summary;

            if (!AssertBuildSucceeded(buildSummary))
            {
                return;
            }

            if (!ignoreStandardizedOutput)
            {
                var state = new ThreadState(buildSummary, buildMetadata, !autoRunPlayer && openStandardizedOutput);
                ThreadPool.QueueUserWorkItem(CreateStandardizedBuildOutput, state);
            }
        }
    }
}
