using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEditor.SettingsManagement;
using UnityEngine;

namespace PainfulSmile.BuildManagement.Local
{
    internal static class LocalSettingsProvider
    {
        private const string DefaultStandardizedBuildOutputPath = "Builds";

        private static readonly GUIContent ChangeButtonLabel = new GUIContent("Change Path");
        private static readonly GUIContent OpenBuiltPlayerModeLabel = new GUIContent("Open Built Player Options");
        private static readonly GUIContent ShowProjectSettingsLabel = new GUIContent("Show Project Settings");
        private static readonly GUIContent StandardizedBuildOutputPathLabel = new GUIContent("Standardized Build Output Path");

        [UserSetting] private static readonly LocalSetting<OpenBuiltPlayerOptions> OpenBuiltPlayerOptionsSetting = new LocalSetting<OpenBuiltPlayerOptions>(BuildManagerUtility.PackageName + ".localBuilds.openBuiltPlayerOptions", OpenBuiltPlayerOptions.OpenBoth);
        [UserSetting] private static readonly LocalSetting<bool> CreateStandardizedBuildOutputSetting = new LocalSetting<bool>(BuildManagerUtility.PackageName + ".localBuilds.createStandardizedBuildOutput", true);
        [UserSetting] private static readonly LocalSetting<bool> GroupByBuildNameSetting = new LocalSetting<bool>(BuildManagerUtility.PackageName + ".localBuilds.groupByBuildName", true);
        [UserSetting] private static readonly LocalSetting<bool> GroupByBuildTargetSetting = new LocalSetting<bool>(BuildManagerUtility.PackageName + ".localBuilds.groupByBuildTarget", true);
        [UserSetting] private static readonly LocalSetting<string> StandardizedBuildOutputPathSetting = new LocalSetting<string>(BuildManagerUtility.PackageName + ".localBuilds.standardizedBuildOutputPath", DefaultStandardizedBuildOutputPath);

        private static Settings s_Settings;

        internal static bool CreateStandardizedBuildOutput => CreateStandardizedBuildOutputSetting;
        internal static bool GroupByBuildName => GroupByBuildNameSetting;
        internal static bool GroupByBuildTarget => GroupByBuildTargetSetting;
        internal static OpenBuiltPlayerOptions OpenBuiltPlayerOptions => OpenBuiltPlayerOptionsSetting;
        internal static string StandardizedBuildOutputPath
        {
            get
            {
                if (StandardizedBuildOutputPathSetting != DefaultStandardizedBuildOutputPath)
                {
                    return StandardizedBuildOutputPathSetting;
                }

                string path = Path.GetDirectoryName(Application.dataPath);

                return Path.Combine(path, DefaultStandardizedBuildOutputPath);
            }
        }

        internal static Settings Settings => s_Settings ?? (s_Settings = new Settings(BuildManagerUtility.PackageName));

        [SettingsProvider]
        private static SettingsProvider CreateSettingsProvider()
        {
            return new UserSettingsProvider(BuildManagerUtility.UserPreferencesPath, Settings, new[] { typeof(LocalSettingsProvider).Assembly });
        }

        [UserSettingBlock(" ")]
        private static void DrawHeaderBlock(string searchContext)
        {
            if (!BuildManagerUtility.TryMatchSearch(searchContext, ShowProjectSettingsLabel.text))
            {
                return;
            }

            if (GUILayout.Button(ShowProjectSettingsLabel))
            {
                SettingsService.OpenProjectSettings(BuildManagerUtility.ProjectSettingsPath);
            }
        }

        [UserSettingBlock("Local Builds")]
        private static void DrawLocalBuildsBlock(string searchContext)
        {
            using (var blockScope = new EditorGUI.ChangeCheckScope())
            {
                if (BuildManagerUtility.TryMatchSearch(searchContext, OpenBuiltPlayerModeLabel.text))
                {
                    OpenBuiltPlayerOptionsSetting.value = (OpenBuiltPlayerOptions)EditorGUILayout.EnumFlagsField(OpenBuiltPlayerModeLabel, OpenBuiltPlayerOptionsSetting);
                    SettingsGUILayout.DoResetContextMenuForLastRect(OpenBuiltPlayerOptionsSetting);
                }

                CreateStandardizedBuildOutputSetting.value = SettingsGUILayout.SettingsToggle("Create Standardized Build Output", CreateStandardizedBuildOutputSetting, searchContext);

                if (blockScope.changed)
                {
                    Settings.Save();
                }
            }

            using (new EditorGUI.DisabledScope(!CreateStandardizedBuildOutputSetting))
            {
                using (var blockScope = new EditorGUI.ChangeCheckScope())
                using (new EditorGUI.IndentLevelScope())
                {
                    GroupByBuildNameSetting.value = SettingsGUILayout.SettingsToggle("Group by Build Name", GroupByBuildNameSetting, searchContext);
                    GroupByBuildTargetSetting.value = SettingsGUILayout.SettingsToggle("Group by Build Target", GroupByBuildTargetSetting, searchContext);

                    if (blockScope.changed)
                    {
                        Settings.Save();
                    }

                    if (!BuildManagerUtility.TryMatchSearch(searchContext, StandardizedBuildOutputPathLabel.text)
                     && !BuildManagerUtility.TryMatchSearch(searchContext, ChangeButtonLabel.text))
                    {
                        return;
                    }

                    Rect position = EditorGUILayout.GetControlRect();
                    position = EditorGUI.PrefixLabel(position, StandardizedBuildOutputPathLabel);

                    if (GUI.Button(position, ChangeButtonLabel, EditorStyles.miniButton))
                    {
                        string value = EditorUtility.OpenFolderPanel("Builds folder location", Path.GetDirectoryName(StandardizedBuildOutputPath), Path.GetFileName(StandardizedBuildOutputPath));

                        if (string.IsNullOrEmpty(value) == false)
                        {
                            StandardizedBuildOutputPathSetting.SetValue(value, true);
                        }
                    }

                    SettingsGUILayout.DoResetContextMenuForLastRect(StandardizedBuildOutputPathSetting);
                    position = EditorGUILayout.GetControlRect();

                    if (Event.current.type == EventType.MouseUp && Event.current.button == 0 && position.Contains(Event.current.mousePosition))
                    {
                        Process.Start(Directory.Exists(StandardizedBuildOutputPath) ? StandardizedBuildOutputPath : Path.GetDirectoryName(Application.dataPath));
                    }

                    string outputLabel = StandardizedBuildOutputPathSetting == DefaultStandardizedBuildOutputPath
                                             ? $"./{DefaultStandardizedBuildOutputPath}"
                                             : StandardizedBuildOutputPath;

                    EditorGUI.LabelField(position, outputLabel, EditorStyles.miniLabel);
                    SettingsGUILayout.DoResetContextMenuForLastRect(StandardizedBuildOutputPathSetting);
                }
            }
        }
    }
}
