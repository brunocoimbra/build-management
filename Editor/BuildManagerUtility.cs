using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace PainfulSmile.BuildManagement
{
    internal static class BuildManagerUtility
    {
        internal const string PackageName = "br.com.painfulsmile.build-management";
        internal const string UserPreferencesPath = "Preferences/Build Manager";
        internal const string ProjectSettingsPath = "Project/Build Manager";

        internal static string BuildName => PackageSettingsProvider.UseProjectFolderAsBuildName
                                                ? Path.GetFileName(Path.GetDirectoryName(Application.dataPath))
                                                : PlayerSettings.productName;

        internal static void EnsureDirectoryExists(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        internal static bool TryMatchSearch(string searchContext, string target)
        {
            if (searchContext == null)
            {
                return true;
            }

            searchContext = searchContext.Trim();

            if (string.IsNullOrEmpty(searchContext))
            {
                return true;
            }

            string[] split = searchContext.Split(' ');

            foreach (string value in split)
            {
                if (!string.IsNullOrEmpty(value) && target.IndexOf(value, StringComparison.InvariantCultureIgnoreCase) > -1)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
