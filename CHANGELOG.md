# Changelog

## [1.2.0] - 2019-11-08

- Added BuildName key on metadata

## [1.1.1] - 2019-10-30

- Added button to easily switch between Project Settings and User Preferences

## [1.1.0] - 2019-10-28

- Added option to override scripting backend when building locally
- Added runtime BuildMetadata class
- Moved UnityCloudBuildManifest to a runtime assembly and exposed it

## [1.0.2] - 2019-10-23

- Changed standardized build output to be created in another thread.
- Changed default OpenBuiltPlayerOptions to OpenBoth.
- Fixed issues with 2019.x

## [1.0.1] - 2019-10-19

- Fixed issues with 2018.4

## [1.0.0] - 2019-10-19

- First version
